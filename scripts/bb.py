import time
from multiprocessing import Pool

my_sleeps = [1, 1, 1, 1, 1, 1]

pool = Pool()  # Define the numebr of cores here
starttime = time.time()
bag_full_of_nothing = pool.map(time.sleep, my_sleeps)
print(list(bag_full_of_nothing))
endtime = time.time()
print(f"Parallel took {endtime-starttime} seconds")

starttime = time.time()
bag_full_of_nothing = map(time.sleep, my_sleeps)
print(list(bag_full_of_nothing))
endtime = time.time()
print(f"Sequential took {endtime-starttime} seconds")

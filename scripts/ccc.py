try:
    roll = int(input("Please enter the result of a d20 roll: "))
except ValueError as e:
    print(f"Got a {e.__class__}.")
    print(e)
else:
    try:
        if roll not in range(1, 20):
            raise ValueError(f"{roll} is an impossible value on a d20")
        else:
            print(f"Rolled a {roll}!")
    except ValueError as e:
        print(f"Got a {e.__class__}.")
        print(e)

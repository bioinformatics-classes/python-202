import re
import time
from collections import defaultdict
from multiprocessing import Pool

def cheap_fasta_reader(fasta_file):
    """
    Parses a FASTA file
    Takes a FASTA file path as input (str)
    Returns a list of DNA sequences (list)
    """
    fasta = open(fasta_file, "r")
    seqs = defaultdict(str)
    for lines in fasta:
        if lines.startswith(">"):
            name = lines
        else:
            seqs[name] += lines.strip()

    seqs = list(seqs.values())

    return seqs


# Define a function to find exons:
def exon_finder(sequence):
    """
    Removes exons from a DNA sequence.
    Takes a DNA sequence (str) as input
    Returns an exonless DNA sequence (str)
    """
    exon = re.search(r"^(\w\w\w)*(ATG)((\w\w\w)*?)((TAG)|(TAA)|(TGA))", sequence)
    try:
        exon = exon.group()
    except AttributeError:
        pass

    return exon


if __name__ == "__main__":
    print("Reading sequences from file...")
    sequences = cheap_fasta_reader("assets/corvus.fasta")
    print("All sequences read! Looking for exons.")
    # Sequential approach
    starttime = time.time()
    exons = map(exon_finder, sequences)
    print(len(list(filter(None, exons))))  # Whaaaat???
    endtime = time.time()
    print(f"Sequential took {endtime-starttime} seconds")

    # Multiprocess approach
    pool = Pool()  # Number of processes can be defined here
    starttime = time.time()
    exons = pool.map(exon_finder, sequences)
    pool.close()
    print(len(list(filter(None, exons))))
    endtime = time.time()
    print(f"Parallel took {endtime-starttime} seconds")

### classes[17] = "Python 202"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

---

### Summary

* &shy;<!-- .element: class="fragment" -->The focus of this class is advanced python programming
* &shy;<!-- .element: class="fragment" -->We will focus on:
    * &shy;<!-- .element: class="fragment" -->Regular expressions
    * &shy;<!-- .element: class="fragment" -->Multiprocessing
    * &shy;<!-- .element: class="fragment" -->`zip()`
    * &shy;<!-- .element: class="fragment" -->`defaultdict()`
    * &shy;<!-- .element: class="fragment" -->CI/CD

&shy;<!-- .element: class="fragment" -->![Dungeon entrance](assets/entrance.jpg)

---

### Regular expressions

![Library](assets/library.jpg)

---

### Regular expressions

* &shy;<!-- .element: class="fragment" -->*Some people, when confronted with a problem, think "I know, I'll use regular expressions.". Now they have two problems.* - Jaime Zawinski
* &shy;<!-- .element: class="fragment" -->`regexps` are **very** powerful ways to find & manipulate text strings 
* &shy;<!-- .element: class="fragment" -->May look somewhat arcane at first, but in time become easy to read
* &shy;<!-- .element: class="fragment" -->Use of *symbols* that have non-literal meaning
* &shy;<!-- .element: class="fragment" -->**Not** a *Python* exclusive - available in many languages

&shy;<!-- .element: class="fragment" -->![A wizard's spell-book](assets/spellbook.png)

|||

### Common regexp symbols

| **Character** | **Meaning**                  |
| ------------- | ---------------------------- |
| \\d           | Any digit                    |
| \\D           | Any non-digit                |
| \\w           | Any word character           |
| \\W           | Any non-word character       |
| \\s           | Any whitespace character     |
| \\S           | Any non-whitespace character |
| .             | Any character                |
| \\            | Escape the next character    |

|||

### Common regexp symbols

| **Quantifier** | **Meaning**                                                  |
| -------------- | ------------------------------------------------------------ |
| +              | One or more                                                  |
| \*             | Zero or more                                                 |
| ?              | Zero or one; After another quantifier, makes it “non-greedy” |
| {i}            | Exactly “i” times                                            |
| {i, j}         | “i” or “j” times                                             |
| {i,}           | “i” or more times                                            |

|||

### Common regexp symbols

| **Anchors** | **Meaning**         |
| ----------- | ------------------- |
| ^           | Beginning of string |
| $           | End of string       |

|||

### Common regexp symbols

| **Logic operator** | **Meaning**                              |
| ------------------ | ---------------------------------------- |
| \|                 | OR                                       |
| (…)                | Make a group                             |
| \[…\]              | Any character inside the parenthesis     |
| \[^…\]             | Any character not inside the parenthesis |

---

### Regular expressions in python

* &shy;<!-- .element: class="fragment" -->`import re`: The most common functions
    * &shy;<!-- .element: class="fragment" -->`re.search()` / `re.match()` - Returns matched string
    * &shy;<!-- .element: class="fragment" -->`re.findall()` - Returns all non-overlapping string matches
    * &shy;<!-- .element: class="fragment" -->`re.sub()` - Returns original string, replacing matched regexp
* &shy;<!-- .element: class="fragment" -->Use of an 'r' prefix is highly recommended!

&shy;<!-- .element: class="fragment" -->[![Cat-bat familiar](assets/catbat.jpg)](https://frankensteinsfunhouse.tumblr.com/post/188249412387)

|||

### Regular expressions in python

``` python
import re

my_string = "The batterd wizard Hadcatz's familiar is a bat!"
familiar = re.search(r"(bat|cat)!$", my_string)  # Returns a `re object`
print(familiar)
print(familiar.group())  # Returns a string!

new_familiar = re.sub(r"bat!$", r"cat!", my_string)  # Returns a string!
print(new_familiar)

many_words = re.findall(r"(.at)", my_string)  # Returns a list of strings!
print(many_words)

everything_in_between = re.search(r"wizard.*bat", my_string)
print(everything_in_between.group())
```

|||

### Want to become a regexp pro?

* Try [this "exam"](https://www.hackerrank.com/domains/regex) on for size!
* Or [this explainer](https://regex101.com/)

---

### Multiprocessing

![A wizard casting mirror image](assets/mirror.png)

---

### Multiprocessing

* &shy;<!-- .element: class="fragment" -->Using multiple CPU cores to simultaneously process different "tasks"
* &shy;<!-- .element: class="fragment" -->Python's `multiprocessing` is only one of the many ways to do this
* &shy;<!-- .element: class="fragment" -->Here we use the simplest abstraction: `multiprocessing.Pool`
* &shy;<!-- .element: class="fragment" -->`Pool()` defines the number of simultaneous processes
* &shy;<!-- .element: class="fragment" -->Then use `Pool.map()` method to distribute & start the processes
* &shy;<!-- .element: class="fragment" -->Beware of threading overheads!

&shy;<!-- .element: class="fragment" -->![Swarm](assets/swarm.png)

|||

### Multiprocessing example

``` python
import time
from multiprocessing import Pool

my_sleeps = [1, 1, 1, 1, 1, 1]

# Sequential version
starttime = time.time()
bag_full_of_nothing = map(time.sleep, my_sleeps)
print(list(bag_full_of_nothing))
endtime = time.time()
print(f"Sequential took {endtime-starttime} seconds")

# Parallel version
starttime = time.time()
pool = Pool()  # Define the numebr of cores here
bag_full_of_nothing = pool.map(time.sleep, my_sleeps)
print(list(bag_full_of_nothing))
endtime = time.time()
print(f"Parallel took {endtime-starttime} seconds")

```

---

### Other neat tricks

![Treasure](assets/treasure.jpg)

---

### `zip()`

* &shy;<!-- .element: class="fragment" -->A python "trick" to iterate over multiple sequences simultaneously
* &shy;<!-- .element: class="fragment" -->It stops as soon as the shortest sequence is done
* &shy;<!-- .element: class="fragment" -->Can be used to pair items
* &shy;<!-- .element: class="fragment" -->Can be used to create `dicts`

&shy;<!-- .element: class="fragment" -->![Twin swords](assets/Starsteel_Twin_Swords.png)

|||

### `zip()` in a for loop

``` python
classes = ["Ranger", "Cleric", "Rogue", "Barbarian"]
armors = ["Leather", "Full plate", "Studded leather", "Chainmail"]
weapons = ["Longbow", "Mace", "Dagger", "Greataxe"]

for char_class, armor, weapon in zip(classes, armors, weapons):
    print("Class", char_class, "-", armor, "armor and", weapon)
```

|||

### `zip()` for pairing items

``` python
# List of tuples
print(list(zip(classes, weapons)))

# Dictionary
print(dict(zip(classes, armors)))
```

---

### `defaultdict()`

* &shy;<!-- .element: class="fragment" -->A python dictionary with a default value for every key
* &shy;<!-- .element: class="fragment" -->It's argument is a *function*
* &shy;<!-- .element: class="fragment" -->Can be used with `int`, `list`, `dict`, or `str`
    * &shy;<!-- .element: class="fragment" -->Useful to avoid exception handling
* &shy;<!-- .element: class="fragment" -->In practice: `def_dict = defaultdict(function)`
* &shy;<!-- .element: class="fragment" -->All `dict` methods are available for `defaultdict`

&shy;<!-- .element: class="fragment" -->![Default image](assets/default.jpeg)

|||

### `defaultdict()` simple example

``` python
from collections import defaultdict

gold_value = defaultdict(int)

gold_value["Malachite"] = 5
print(gold_value)
gold_value["Diamond"]
print(gold_value)
```

|||

### `defaultdict()` using a function

``` python
from random import choice
from collections import defaultdict

def roll_3_die():
    """Rolls 3 six sided die and returns the total"""
    total = choice(range(1,7)) + choice(range(1,7)) + choice(range(1,7))
    
    return total

char_stats = defaultdict(roll_3_die, {"Str": 12, "Dex": 15, "Con": 12})
print(char_stats)
print(char_stats["Wis"])
print(char_stats["Int"])
print(char_stats["Cha"])
print(char_stats)
```

|||

### `defaultdict()` default constant

```python
# Using a `defaultdict()`
from collections import defaultdict

monster_powers = defaultdict(lambda: "Unknown", {"Basilisk": "Flesh to stone", "Dragon": "Firebreathing"})

print(monster_powers)
print(monster_powers["Basilisk"])
print(monster_powers["Hydra"])
```

---

### CI/CD

* &shy;<!-- .element: class="fragment" -->Who never delivered code that should be working fine be wasn't?
* &shy;<!-- .element: class="fragment" -->Human error can and will happen, even to the most diligent
* &shy;<!-- .element: class="fragment" -->How can this be mitigated?
    * &shy;<!-- .element: class="fragment" -->Automation: CI/CD

&shy;<!-- .element: class="fragment" -->![A rogue failing to disarm a trap](assets/trap_fail.jpg)

|||

### What is a CI/CD pipeline?

* A series of steps performed on an event:
    * On each commit (Unit/integration testing)
    * On new releases (Deployment)
* The pipeline's steps can be executed manually but CI/CD automated (and less error prone)

&shy;<!-- .element: class="fragment" -->![A D&D automaton](assets/automaton.jpg)

|||

### CI/CD example

* Just clone the example repository and let's have a look:

``` bash
cd ~/
git clone https://gitlab.com/bioinformatics-classes/ci-example.git
```

* &shy;<!-- .element: class="fragment" -->Remember this example?

&shy;<!-- .element: class="fragment" -->![A Calvin & Hobbes comic about D&D](assets/calvin.jpg)

|||

### Special notes

* &shy;<!-- .element: class="fragment" -->Each online platform has its own way to deal with CI/CD
    * &shy;<!-- .element: class="fragment" -->[Gitlab](https://docs.gitlab.com/ee/ci/)
    * &shy;<!-- .element: class="fragment" -->[Github](https://docs.github.com/en/actions)
    * &shy;<!-- .element: class="fragment" -->[Bitbucket](https://bitbucket.org/product/features/pipelines)

&shy;<!-- .element: class="fragment" -->![A collection of D&D books](assets/books.jpg)

---

### Your task for today

* Given a FASTA file:
    * Get whether a list of DNA sequences contain exons
        * (Start with `ATG` ends with `TAG|TAA|TGA`) <- Hint hint!
* Use the techniques learnt here for finding the exons
    * You don't have to use **all** of them, just make the best of it
* Get a test file like this (large enough to be worth multiprocessing, but small enough to be quickly processed):

``` bash
wget https://gitlab.com/bioinformatics-classes/python-202/raw/master/assets/corvus.fasta
```

* In the slide below, you will find a possible solution to the problem. Use only in case of emergency!

|||

### A possible solution

``` python
import re
import time
from collections import defaultdict
from multiprocessing import Pool

def cheap_fasta_reader(fasta_file):
    """
    Parses a FASTA file
    Takes a FASTA file path as input (str)
    Returns a list of DNA sequences (list)
    """
    fasta = open(fasta_file, "r")
    seqs = defaultdict(str)
    for lines in fasta:
        if lines.startswith(">"):
            name = lines
        else:
            seqs[name] += lines.strip()

    seqs = list(seqs.values())

    return seqs


def exon_finder(sequence):
    """
    Finds an exon in a DNA sequence.
    Takes a DNA sequence (str) as input
    Returns an exon DNA sequence (str) or None, if not found
    """
    exon = re.search(r"^(\w\w\w)*(ATG)((\w\w\w)*?)((TAG)|(TAA)|(TGA))", sequence)
    try:
        exon = exon.group()
    except AttributeError:
        pass

    return exon


if __name__ == "__main__":
    print("Reading sequences from file...")
    sequences = cheap_fasta_reader("assets/corvus.fasta")
    print("All sequences read! Looking for exons.")
    # Sequential approach
    starttime = time.time()
    exons = map(exon_finder, sequences)
    print(len(list(filter(None, exons))))  # Whaaaat???
    endtime = time.time()
    print(f"Sequential took {endtime-starttime} seconds")

    # Multiprocess approach
    pool = Pool()  # Number of processes can be defined here
    starttime = time.time()
    exons = pool.map(exon_finder, sequences)
    pool.close()
    print(len(list(filter(None, exons))))
    endtime = time.time()
    print(f"Parallel took {endtime-starttime} seconds")
```

---

### Alternative task:

* Develop a CI/CD pipeline for homework 2
    * Select one of the online platforms and automate the test running
* If you really want a challenge:
    * Create an integration test
    * After running unit tests, run your program
    * Make sure the resulting Nexus file runs in MrBayes
        * In the pipeline

---

### References

* [Custom exception handling](https://www.programiz.com/python-programming/user-defined-exception)
* [Regular expressions' good practices](https://blog.codinghorror.com/regular-expressions-now-you-have-two-problems/)
* [Python `re` module](https://docs.python.org/3/library/re.html)
* [Introducing python](https://www.oreilly.com/library/view/introducing-python-2nd/9781492051374/)
* [Python `collections`](https://docs.python.org/3/library/collections.html)
* [An in depth view at Python multiprocessing](https://www.codesdope.com/blog/article/multiprocessing-using-pool-in-python/)
* [Python's `multiprocessing` module](https://docs.python.org/3/library/multiprocessing.html)
* [What is a CI/CD pipeline?](https://www.redhat.com/en/topics/devops/what-cicd-pipeline)
